/* eslint-disable prettier/prettier */
import { createRouter, createWebHistory } from "vue-router";
//import App from "../App";

const routes = [{
        path: "/",
        name: "mainpage",
        component: () =>
            import ("../views/mainpage.vue"),
    },
    {
        path: "/singup",
        name: "singup",
        component: () =>
            import ("../views/singup.vue"),
    },

    {
        path: "/login",
        name: "login",
        component: () =>
            import ("../views/login.vue"),
    },


];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;