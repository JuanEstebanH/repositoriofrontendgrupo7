/* eslint-disable prettier/prettier */

import { createApp } from "vue";
import "@fortawesome/fontawesome-free/js/fontawesome";
import "@fortawesome/fontawesome-free/js/solid";
import "@fortawesome/fontawesome-free/js/regular";
import "@fortawesome/fontawesome-free/js/brands";
import App from "./App.vue";
import router from './router'
createApp(App).use(router).mount('#app')